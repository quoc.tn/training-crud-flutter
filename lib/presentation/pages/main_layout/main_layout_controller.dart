import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/presentation/pages/home/home_page.dart';

class MainLayoutController extends GetxController {
  int currentIndex = 0;
  final List<Widget> screens = [
    const HomePage(),
    SearchPage(),
    SettingPage(),
    ProfilePage()
  ];
  void changeCurrentIndex(int index){
    currentIndex = index;
    update();
  }
}

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Search',
        style: TextStyle(fontSize: 24,
        color: Colors.black),
      ),
    );
  }
}

class SettingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Setting',
        style: TextStyle(fontSize: 24,
        color: Colors.black),
      ),
    );
  }
}

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Profile',
        style: TextStyle(fontSize: 24,
        color: Colors.black),
      ),
    );
  }
}
