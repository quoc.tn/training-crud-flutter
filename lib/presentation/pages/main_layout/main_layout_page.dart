import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/core/export/core_export.dart';
import 'package:flutter/cupertino.dart';
import 'package:template/presentation/pages/main_layout/main_layout_controller.dart';

class MainLayout extends GetView<MainLayoutController>{
  const MainLayout({super.key});


  @override
  Widget build(BuildContext context) {
    Get.put(MainLayoutController());
    return GetBuilder<MainLayoutController>(builder: (controller) {
      return Scaffold(
      //appBar: AppBar(),
      body: Stack(
        children: [
          Positioned.fill(
            child: ClipRect(
              child: Container(
                color: Colors.white, // Màu nền của container
              ),
            ),
          ),
          controller.screens[controller.currentIndex],
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: controller.currentIndex,
        onTap: (index) {
          controller.changeCurrentIndex(index);
        },
        selectedItemColor: ColorResources.YELLOW_FOCUS,
        unselectedItemColor: ColorResources.WHITE,
        backgroundColor: ColorResources.BG_COLOR,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.archivebox),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.settings_solid),
            label: 'Setting',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.person),
            label: 'Profile',
          ),
        ],
      ),
    );
    }
    );
  }
}



