import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:template/core/utils/color_resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:template/presentation/pages/home/home_controller.dart';
import 'package:template/presentation/pages/home/widgets/student/add_student/add_student.dart';
import 'package:template/presentation/pages/home/widgets/student/students_tab/students_tab.dart';
import 'package:template/presentation/pages/home/widgets/subject/add_subject/add_subject.dart';
import 'package:template/presentation/pages/home/widgets/subject/subjects_tab/subjects_tab.dart';
import 'package:template/presentation/pages/home/widgets/tab_item/tab_item.dart';
import 'widgets/student/edit_student/edit_student.dart';
import 'widgets/subject/edit_subject/edit_subject.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorResources.WHITE,
        body: Stack(children: [
          Positioned.fill(
            top: 119.h,
            left: 20.w,
            child: Container(
              height: 35.h,
              width: 285.w,
              decoration: BoxDecoration(
                color: ColorResources.WHITE,
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: ColorResources.GREY_BG, width: 1.w),
              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Enter Keyword to find out',
                  fillColor: ColorResources.BG,
                  filled: true,
                  contentPadding: EdgeInsets.only(
                    left: 60.w,
                    right: 30.w,
                    top: 8.h,
                    bottom: 8.h,
                  ),
                  border: const OutlineInputBorder(
                    borderSide: BorderSide.none, // Loại bỏ gạch dưới
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              top: 56.h,
              left: 78.w,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'STUDENT',
                      style: TextStyle(
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold,
                        color: ColorResources.YELLOW_FOCUS,
                      ),
                    ),
                    Text(
                      'MANAGEMENT',
                      style: TextStyle(
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold,
                        color: ColorResources.BG_COLOR,
                      ),
                    ),
                  ])),
          Positioned(
              top: 180.h,
              left: 21.w,
              child: Obx(() => Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TabItem(
                    index: 0,
                    title: "Student",
                    selectedIndex: controller.selectedIndex.value,
                    onTap: controller.onTabSelected,
                  ),
                  TabItem(
                    index: 1,
                    title: "Subjects",
                    selectedIndex: controller.selectedIndex.value,
                    onTap: controller.onTabSelected,
                  ),
                  TabItem(
                    index: 2,
                    title: "Evolation",
                    selectedIndex: controller.selectedIndex.value,
                    onTap: controller.onTabSelected,
                  ),
                  TabItem(
                    index: 3,
                    title: "Event",
                    selectedIndex: controller.selectedIndex.value,
                    onTap: controller.onTabSelected,
                  ),
                ],
              )
          )),
          Positioned(
              top: 227.h,
              right: 0,
              left: 6.w, // Điều chỉnh vị trí bắt đầu của nội dung bên dưới
              child: Column(
                children: [
                  SizedBox(
                      height: 574.h,
                      child: Obx(() => IndexedStack(
                        index: controller.selectedIndex.value,
                        children: [
                          const StudentsTab(),
                          const SubjectsTab(),
                          // Nội dung tương ứng với tab 2
                          Center(
                            child: Text(
                              'Evaluation',
                              style: TextStyle(
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                                color: ColorResources.BG_COLOR,
                              ),
                            ),
                          ),

                          // Nội dung tương ứng với tab 3
                          Center(
                            child: Text(
                              'Events',
                              style: TextStyle(
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                                color: ColorResources.BG_COLOR,
                              ),
                            ),
                          ),
                        ],
                      )),
              )],
              )),
          Positioned(
              top: 122.h,
              left: -2.w,
              child: Obx(() => Visibility(
                  visible: controller.isShowAddPage.value,
                  child: const AddStudent()))),
          Positioned(
              top: 122.h,
              left: -2.w,
              child: Obx(() => Visibility(
                  visible: controller.isShowEditPage.value,
                  child: const EditStudent()))),
          Positioned(
              top: 122.h,
              left: -2.w,
              child: Obx(() => Visibility(
                  visible: controller.isShowSubjectAddPage.value,
                  child: const AddSubject())),
          ),
          Positioned(
              top: 122.h,
              left: -2.w,
              child: Obx (()=> Visibility(
                  visible: controller.isShowSubjectEditPage.value,
                  child: const EditSubject())),
          ),
          Positioned(
            top: 33.h,
            left: 5.w,
            child: Image.asset(
              'assets/images/logo.png',
              height: 121.h,
              width: 76.h,
              fit: BoxFit.cover,
            ),
          ),
        ]));
  }
}
