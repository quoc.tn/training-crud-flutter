
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:template/core/export/core_export.dart';
import 'package:template/data/model/student/student_model.dart';
import 'package:template/data/repositories/student_repository.dart';
import 'package:template/data/repositories/subject_respository.dart';

import '../../../data/model/subject/subject_model.dart';

class HomeController extends GetxController with WidgetsBindingObserver {
  final StudentRespository _studentRespository =
      GetIt.I.get<StudentRespository>();
  final SubjectRespository _subjectRespository =
      GetIt.I.get<SubjectRespository>();
  RxList<Student> studentList = RxList<Student>();
  RxList<Subject> subjectList = RxList<Subject>();
  RxBool isLoadingStudent = true.obs;
  RxBool isLoadingSubject = true.obs;

  RxInt selectedIndex = 0.obs;
  RxString selectedItem = 'View all'.obs;

  TextEditingController controllerFullName = TextEditingController();
  TextEditingController controllerId = TextEditingController();
  TextEditingController controllerClassName = TextEditingController();
  TextEditingController controllerDateOfBirth = TextEditingController();
  TextEditingController controllerrAaverageScore = TextEditingController();
  TextEditingController controllerAddress = TextEditingController();
  TextEditingController controllerPhoneNumber = TextEditingController();
  TextEditingController controllerEmail = TextEditingController();

  TextEditingController controllerSchoolTerm = TextEditingController();
  TextEditingController controllerQuantityCredits = TextEditingController();

  RxBool isShowAddPage = false.obs;
  RxBool isShowEditPage = false.obs;

  RxBool isShowSubjectAddPage = false.obs;
  RxBool isShowSubjectEditPage = false.obs;


  RxList<String> registeringStatus = ['CONFIRMED', 'AWAITING', 'DECLINED'].obs;
  RxString registerStatus = 'CONFIRMED'.obs;
 
  RxList<String> registeredCourses = RxList<String>(); // Danh sách các môn học đã đăng kí
  RxList<String> availableCourses = [
    "Giải tích 2",
    "Kỹ thuật Lập trình",
    "Đại số tuyến tính",
    "Triết học Mac Lê nin",
    "Tư tưởng HCM",
  ].obs;

  Student? studentEdit;
  Subject? subjectEdit;
  


  void onChangeCheckBox(String course) {
    registeredCourses.value.contains(course)
        ? registeredCourses.value.remove(course)
        : registeredCourses.value.add(course);
  }

  void changeStateShowAddPage(bool isShow) {
    isShowAddPage.value = isShow;
    
  }

  void changeStateShowSubjectAddPage(bool isShow) {
    isShowSubjectAddPage.value = isShow;
  }

  void changeStateShowSubjectEditPage(bool isShow) {
    isShowSubjectEditPage.value = isShow;
  }

  void changeStateShowEditPage(bool isShow) {
    isShowEditPage.value = isShow;
  }

  void initEditStudent(Student student) {
    controllerFullName.text = student.fullName;
    controllerId.text = student.id;
    controllerClassName.text = student.className;
    controllerDateOfBirth.text = student.dateOfBirth.toString();
    controllerrAaverageScore.text = student.averageScore.toString();
    controllerAddress.text = student.contactInfo.address;
    controllerPhoneNumber.text = student.contactInfo.phoneNumber;
    controllerEmail.text = student.contactInfo.email;
    registeredCourses.value = student.registeredCourses;
    studentEdit = student;
  }

  void initEditSubject(Subject subject){
    controllerFullName.text = subject.fullName;
    controllerClassName.text=subject.className;
    controllerId.text=subject.studentId;
    controllerQuantityCredits.text = subject.creditHours.toString();
    controllerSchoolTerm.text=subject.semester;
    registeredCourses.value=subject.registeredCourses;
    subjectEdit=subject;
    registerStatus.value=subject.registrationStatus;
  }

  bool validateStudent(BuildContext context) {
    if (IZIValidate.nullOrEmpty(controllerFullName.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Fullname', ColorResources.RED);
      return false;
    }
    if (IZIValidate.nullOrEmpty(controllerClassName.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Class', ColorResources.RED);
      return false;
    }
    if (!IZIValidate.isValidEmail(controllerEmail.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Email', ColorResources.RED);
      return false;
    }
    if (IZIValidate.nullOrEmpty(controllerDateOfBirth.text) ||
        double.tryParse(controllerDateOfBirth.text) == null) {
      IziShowDialog.showSnackBar(context, 'Invalid Date of Birth', ColorResources.RED);
      return false;
    }
    if (IZIValidate.nullOrEmpty(controllerAddress.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Address', ColorResources.RED);
      return false;
    }
    if (!IZIValidate.phoneNumber(controllerPhoneNumber.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Phone number', ColorResources.RED);
      return false;
    }
    if (IZIValidate.nullOrEmpty(controllerrAaverageScore.text) ||
        double.tryParse(controllerrAaverageScore.text) == null) {
      IziShowDialog.showSnackBar(context, 'Invalid Aaverage Mark', ColorResources.RED);
      return false;
    }
    return true;
  }

  bool validateSubject(BuildContext context) {
    if (IZIValidate.nullOrEmpty(controllerFullName.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Fullname', ColorResources.RED);
      return false;
    }

    if (IZIValidate.nullOrEmpty(controllerClassName.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid Class', ColorResources.RED);
      return false;
    }

    if (IZIValidate.nullOrEmpty(controllerSchoolTerm.text)) {
      IziShowDialog.showSnackBar(context, 'Invalid School term', ColorResources.RED);
      return false;
    }
    if (IZIValidate.nullOrEmpty(controllerQuantityCredits.text) || int.tryParse(controllerQuantityCredits.text)==null) {
      IziShowDialog.showSnackBar(context, 'Invalid Quantity of credits', ColorResources.RED);
      return false;
    }
    return true;
  }

  @override
  void onInit() {
    super.onInit();
    //print("Ok");
    fetchStudent();
    fetchSubject();
  }

  void onTabSelected(int index) {
    selectedIndex.value = index;
  }

  void handleChangeDropDownButton(value) {
    selectedItem.value = value ?? 'View all';
  }

  void fetchStudent() {
    _studentRespository.get(
        onSuccess: (data) {
          studentList.value = data;
          isLoadingStudent.value = false;
          
        },
        onError: (e) {});
  }

  void fetchSubject() {
    _subjectRespository.get(onSuccess: (data) {
      subjectList.value = data;
      //print(data);
      isLoadingSubject.value = false;
    }, onError: (e) {
      // print(e);
    });
  }

  void deleteStudent(String id, BuildContext context) {
    _studentRespository.delete(
        id: id,
        onSuccess: () {
          
          IziShowDialog.showSnackBar(context, 'Delete Successful', ColorResources.GREEN);
        },
        onError: (e) {
          IziShowDialog.showSnackBar(context, 'Delete Fail', ColorResources.RED);
        });
  }

  void deleteSubject(String id, BuildContext context) {
    _subjectRespository.delete(
        id: id,
        onSuccess: () {
          
          IziShowDialog.showSnackBar(context, 'Delete Successful', ColorResources.GREEN);
        },
        onError: (e) {
          IziShowDialog.showSnackBar(context, 'Delete Fail', ColorResources.RED);
        });
  }

  ///
  /// Thêm sinh viên
  ///
  void addStudent(BuildContext context) {
    Map<String, dynamic> studentJson = {
      "contactInfo": {
        "address": controllerAddress.text,
        "email": controllerEmail.text,
        "phoneNumber": controllerPhoneNumber.text
      },
      "registeredCourses": registeredCourses,
      "averageScore": double.parse(controllerrAaverageScore.text),
      "dateOfBirth": double.parse(controllerDateOfBirth.text),
      "class": controllerClassName.text,
      "fullName": controllerFullName.text,
    };
    _studentRespository.add(
        data: studentJson,
        onSuccess: () {
          fetchStudent();
          resetStudentForm();

          changeStateShowAddPage(false);
          IziShowDialog.showSnackBar(context, 'Add Successful', ColorResources.GREEN);
        },
        onError: (e) {
          IziShowDialog.showSnackBar(context, 'Add Fail', ColorResources.RED);
        });
  }

  void addSubject(BuildContext context) {
    Map<String, dynamic> subjectJson = {
      "registrationStatus": registerStatus.value,
      "registeredCourses": registeredCourses,
      "semester": controllerSchoolTerm.text,
      // "dateOfBirth": double.parse(controllerDateOfBirth.text),
      "creditHours": int.parse(controllerQuantityCredits.text),
      "class": controllerClassName.text,
      "fullName": controllerFullName.text,
    };
        // print(jsonEncode(subjectJson));

    _subjectRespository.add(
        data: subjectJson,
        onSuccess: () {
          fetchSubject();
          resetSubjectForm();

          changeStateShowSubjectAddPage(false);
          IziShowDialog.showSnackBar(context, 'Add Successful', ColorResources.GREEN);
        },
        onError: (e) {
          IziShowDialog.showSnackBar(context, 'Add Fail', ColorResources.RED);
        });
  }

  void resetStudentForm() {
    controllerFullName.text = '';
    controllerId.text = '';
    controllerClassName.text = '';
    controllerDateOfBirth.text = '';
    controllerrAaverageScore.text = '';
    controllerAddress.text = '';

    controllerPhoneNumber.text = '';
    controllerEmail.text = '';
    registeredCourses.value = [];
  }

  void resetSubjectForm() {
    controllerFullName.text = '';
    controllerId.text = '';
    controllerClassName.text = '';
    controllerSchoolTerm.text = '';
    controllerQuantityCredits.text = '';
    registerStatus.value = 'CONFIRMED';
    registeredCourses.value = [];
  }

  void updateStudent(BuildContext context) {
    Map<String, dynamic> studentJson = {
      "contactInfo": {
        '_id': studentEdit!.contactInfo.id,
        "address": controllerAddress.text,
        "email": controllerEmail.text,
        "phoneNumber": controllerPhoneNumber.text
      },
      "registeredCourses": registeredCourses,
      "averageScore": double.parse(controllerrAaverageScore.text),
      "dateOfBirth": double.parse(controllerDateOfBirth.text),
      "class": controllerClassName.text,
      "fullName": controllerFullName.text,
    };
    _studentRespository.update(
        id: studentEdit!.id,
        data: studentJson,
        onSuccess: () {
          fetchStudent();
          resetStudentForm();

          IziShowDialog.showSnackBar(context, 'Update Successful', ColorResources.GREEN);
          changeStateShowEditPage(false);
        },
        onError: (e) {});
  }

  void updateSubject(BuildContext context){
   Map<String, dynamic> subjectJson = {
      "registrationStatus": registerStatus.value,
      "registeredCourses": registeredCourses,
      "semester": controllerSchoolTerm.text,
      "creditHours": int.parse(controllerQuantityCredits.text),
      "class": controllerClassName.text,
      "fullName": controllerFullName.text,
    };
    _subjectRespository.update(
        id: subjectEdit!.id,
        data: subjectJson,
        onSuccess: () {
          fetchSubject();
          resetSubjectForm();
          IziShowDialog.showSnackBar(context, 'Update Successful', ColorResources.GREEN);
          changeStateShowSubjectEditPage(false);
        },
        onError: (e) {});
  }
}
