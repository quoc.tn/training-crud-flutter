import 'package:flutter/material.dart';
import 'package:template/core/utils/color_resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class TabItem extends StatefulWidget {
  final String title;
  final int index;
  final int selectedIndex;
  final void Function(int) onTap;

  const TabItem({
    Key? key,
    required this.index,
    required this.title,
    required this.selectedIndex,
    required this.onTap,
  }) : super(key: key);

  @override
  State<TabItem> createState() => _TabItemState();
}

class _TabItemState extends State<TabItem> {
  @override
  Widget build(BuildContext context) {
    bool isSelected = widget.selectedIndex == widget.index;
    Color tabColor =
        isSelected ? ColorResources.BG_COLOR : ColorResources.WHITE;
    Color textColor = isSelected ? ColorResources.WHITE : ColorResources.GREY;
    return GestureDetector(
      onTap: () {
        widget.onTap(widget.index);
      },

      child: Container(
        //width: 75,
        constraints: const BoxConstraints(minHeight: 25),
        padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 18.w),
        decoration: BoxDecoration(
          color: tabColor,
          borderRadius: BorderRadius.circular(30.r),
        ),
        child: Text(
          widget.title,
          style: TextStyle(
            fontSize: 12.sp,
            color: textColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

// class TabItem extends StatefulWidget {
//   final String title;
//   final int index;
//   final int selectedIndex;
//   final void Function(int) onTap;

//   const TabItem({
//     Key? key,
//     required this.index,
//     required this.title,
//     required this.selectedIndex,
//     required this.onTap,
//   }) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     bool isSelected = selectedIndex == index;
//     Color tabColor =
//         isSelected ? ColorResources.BG_COLOR : ColorResources.WHITE;
//     Color textColor = isSelected ? ColorResources.WHITE : ColorResources.GREY;
//     return GestureDetector(
//       onTap: () => onTap(index),
//       child: Container(
//         //width: 75,
//         constraints: BoxConstraints(minHeight: 25),
//         padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 18.w),
//         decoration: BoxDecoration(
//           color: tabColor,
//           borderRadius: BorderRadius.circular(30.r),
//         ),
//         child: Text(
//           title,
//           style: TextStyle(
//             fontSize: 12.sp,
//             color: textColor,
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//     );
//   }
// }
