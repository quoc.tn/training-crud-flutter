import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:template/core/utils/color_resources.dart';
import 'package:template/data/export/data_export.dart';
import 'package:template/presentation/pages/home/home_controller.dart';

class StudentItem extends GetView<HomeController> {
  const StudentItem({
    super.key,
    required this.student,
  });

  final Student student;
  
  void showDeleteConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return Container(
          height: 241.h,
          width: 341.w,
          padding: const EdgeInsets.all(0),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.r),
            ),
            content: Text('Are you sure to delete this student?',
                style: TextStyle(
                  fontSize: 16.sp,
                  color: ColorResources.BG_COLOR,
                )),
            actions: [
              TextButton(
                onPressed: () {
                  // Gọi phương thức deleteStudent trong HomeController
                  controller.deleteStudent(student.id, context);
                  controller.studentList.remove(student);
                  Navigator.of(dialogContext).pop(); // Đóng hộp thoại
                },
                child: const Text('Yes'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(dialogContext).pop(); // Đóng hộp thoại
                },
                child: const Text('No'),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.h,
      // width: 375.w,
      child: Stack(children: [
        Container(
            height: 50.h,
            // width: 335.w,
            padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 24.w),
            margin: EdgeInsets.symmetric(vertical: 0.h, horizontal: 14.w),
            //margin: EdgeInsets.symmetric(vertical: 6.h, horizontal: 6.w),
            decoration: BoxDecoration(
              color: ColorResources.GREY_BG,
              borderRadius: BorderRadius.circular(10.r),
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        student.fullName,
                        style: TextStyle(
                          fontSize: 16.sp,
                          color: ColorResources.BLACK,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      
                      Text(
                        student.className,
                        style: TextStyle(
                            fontSize: 14.sp, color: ColorResources.GREY),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: (){
                          controller.initEditStudent(student);
                          controller.changeStateShowEditPage(true);
                        },
                        child: Icon(
                          Icons.edit,
                          color: ColorResources.BG_COLOR,
                          size: 15.w,
                        ),
                      ),
                      SizedBox(width: 8.w),
                      GestureDetector(
                        onTap: () {
                          showDeleteConfirmationDialog(context);
                        },
                        child: Icon(
                          CupertinoIcons.delete,
                          color: ColorResources.YELLOW_FOCUS,
                          size: 15.w,
                        ),
                      ),
                    ],
                  )
                ])),
        Positioned(
          top: 18.h,
          left: 0,
          child: Container(
            height: 23.h,
            width: 26.w,
            decoration: BoxDecoration(
              color: ColorResources.BG_COLOR,
              borderRadius: BorderRadius.circular(5.r),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                student.averageScore.toString(),
                style: TextStyle(
                  fontSize: 12.sp,
                  color: ColorResources.WHITE,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
