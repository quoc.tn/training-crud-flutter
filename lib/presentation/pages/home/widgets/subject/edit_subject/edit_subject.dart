import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:template/presentation/pages/home/home_controller.dart';
import '../../../../../../core/utils/color_resources.dart';
import 'package:flutter/cupertino.dart';

class EditSubject extends GetView<HomeController> {
  const EditSubject({super.key});
  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    void openCourseSelectionDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return GetBuilder<HomeController>(builder: (controller) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(32.r),
                    bottomLeft: Radius.circular(32.r)),
              ),
              contentPadding: const EdgeInsets.all(0),
              title: const Text('Choose subjects',
                  style: TextStyle(color: ColorResources.BG_COLOR)),
              content: Container(
                padding: EdgeInsets.only(top: 16.h),
                height: 300.h,
                width: 400.w,
                // padding: EdgeInsets.all(0),
                child: SingleChildScrollView(
                  child: Wrap(
                    spacing: 8.h,
                    runSpacing: 8.0.w,
                    children: controller.availableCourses.map((course) {
                      return SizedBox(
                        width: 140.w,
                        height: 60.h,
                        child: ListTile(
                          leading: Checkbox(
                              activeColor: ColorResources.YELLOW_TEXT,
                              checkColor: ColorResources.YELLOW_TEXT,
                              visualDensity: VisualDensity.standard,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                    5.0), // Đường viền cho checkbox
                                side: const BorderSide(
                                    color: ColorResources.YELLOW_TEXT),
                                // Viền cho checkbox
                              ),
                              value:
                                  controller.registeredCourses.contains(course),
                              onChanged: (bool? value) {
                                controller.onChangeCheckBox(course);
                              }),
                          title: Text(
                            course,
                            style: const TextStyle(
                              color: ColorResources.YELLOW_TEXT,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          onTap: () {
                            controller.onChangeCheckBox(course);
                          },
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
              actions: <Widget>[
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        ColorResources.YELLOW_BTN),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                    minimumSize:
                        MaterialStateProperty.all<Size>(Size(76.w, 40.h)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.21.r),
                      ),
                    ),
                  ),
                  child: Text('Save',
                      style: TextStyle(
                        fontSize: 10.sp,
                      )),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(ColorResources.WHITE),
                    minimumSize:
                        MaterialStateProperty.all<Size>(Size(76.w, 40.h)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.21.r),
                      ),
                    ),
                  ),
                  child: Text('Cancel',
                      style: TextStyle(
                        fontSize: 10.sp,
                      )),
                ),
              ],
            );
          });
        },
      );
    }
    return GetBuilder<HomeController>(builder: (controller) {
      return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(46.r),
                topRight: Radius.circular(46.r)),
            color: ColorResources.BG_BLUE_COLOR,
          ),
          //color: ColorResources.BG_COLOR,
          width: 377.w,
          height: 695.h,
          alignment: Alignment.centerRight,
          child: Stack(
            //mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Positioned(
                top: 34.h,
                left: 190.w,
                child: Text('UPDATE SUBJECT',
                    //textAlign: TextAlign.right,
                    style: TextStyle(
                      color: ColorResources.BLUE_TITLE,
                      fontSize: 24.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                top: 93.h,
                left: 23.w,
                child: Column(
                  children: [
                    Text('Fullname',
                        //textAlign: TextAlign.right,
                        style: TextStyle(
                          color: ColorResources.WHITE,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.bold,
                        )),
                  ],
                ),
              ),
              Positioned(
                  top: 120.h,
                  left: 23.w,
                  child: SizedBox(
                    width: 323.w,
                    height: 39.h,
                    child: TextField(
                      controller: controller.controllerFullName,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        hintText: 'Enter fullname',
                        filled: true,
                        fillColor: ColorResources.WHITE,
                        hintStyle: TextStyle(color: Colors.grey),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      style: const TextStyle(color: ColorResources.BLACK),
                    ),
                  )),
              Positioned(
                top: 194.h,
                left: 23.w,
                child: Text('Class',
                    //textAlign: TextAlign.right,

                    style: TextStyle(
                      color: ColorResources.WHITE,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                  top: 221.h,
                  left: 23.w,
                  child: SizedBox(
                    width: 175.w,
                    height: 39.h,
                    child: TextField(
                      controller: controller.controllerClassName,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        hintText: 'Enter class',
                        filled: true,
                        fillColor: ColorResources.WHITE,
                        hintStyle: TextStyle(color: Colors.grey),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      style: const TextStyle(color: ColorResources.BLACK),
                    ),
                  )),
              Positioned(
                top: 194.h,
                left: 214.w,
                child: Text('Student ID',
                    //textAlign: TextAlign.right,
                    style: TextStyle(
                      color: ColorResources.WHITE,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                  top: 221.h,
                  left: 214.w,
                  child: SizedBox(
                    width: 131.w,
                    height: 39.h,
                    child: TextField(
                      controller: controller.controllerId,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        hintText: 'Enter ID',
                        filled: true,
                        fillColor: ColorResources.WHITE,
                        hintStyle: TextStyle(color: Colors.grey),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      style: const TextStyle(color: ColorResources.BLACK),
                    ),
                  )),
              Positioned(
                top: 295.h,
                left: 23.w,
                child: Text('School term',
                    //textAlign: TextAlign.right,
                    style: TextStyle(
                      color: ColorResources.WHITE,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                  top: 322.h,
                  left: 23.w,
                  child: SizedBox(
                    width: 199.w,
                    height: 39.h,
                    child: TextField(
                      controller: controller.controllerSchoolTerm,
                      decoration: const InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        hintText: 'Enter School term',
                        filled: true,
                        fillColor: ColorResources.WHITE,
                        hintStyle: TextStyle(color: Colors.grey),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      style: const TextStyle(color: ColorResources.BLACK),
                    ),
                  )),
              Positioned(
                top: 295.h,
                left: 233.w,
                child: Text('Quantity of credits',
                    //textAlign: TextAlign.right,
                    style: TextStyle(
                      color: ColorResources.WHITE,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                  top: 322.h,
                  left: 233.w,
                  child: SizedBox(
                    width: 110.w,
                    height: 39.h,
                    child: TextField(
                      controller: controller.controllerQuantityCredits,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 10.w),
                        hintText: 'Enter Quantity',
                        filled: true,
                        fillColor: ColorResources.WHITE,
                        hintStyle: const TextStyle(color: ColorResources.GREY),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.r)),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      style: const TextStyle(color: ColorResources.BLACK),
                    ),
                  )),
              Positioned(
                top: 396.h,
                left: 23.w,
                child: Text('Registering status',
                    //textAlign: TextAlign.right,
                    style: TextStyle(
                      color: ColorResources.WHITE,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                  top: 423.h,
                  left: 23.w,
                  child: Container(
                      padding: EdgeInsets.only(left: 8.w),
                      //color: ColorResources.WHITE,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.r)),
                        color: ColorResources.WHITE,
                      ),
                      width: 323.w,
                      height: 39.h,
                      child: Align(
                        alignment: Alignment.center,
                        child: DropdownButtonFormField<String>(
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.zero,
                            ),
                            iconEnabledColor: ColorResources.BLACK,
                            //dropdownColor: ColorResources.WHITE,
                            focusColor: ColorResources.GREY,
                            iconDisabledColor: ColorResources.BLACK,
                            value: controller.registerStatus.value,
                            onChanged: (String? value) {
                              controller.registerStatus = value!.obs;
                            },
                            items: controller.registeringStatus.map((index) {
                              return DropdownMenuItem<String>(
                                value: index,
                                child: Text(index),
                              );
                            }).toList()),
                      ))),
              Positioned(
                top: 486.h,
                left: 23.w,
                child: Text('List registed subjects',
                    //textAlign: TextAlign.right,
                    style: TextStyle(
                      color: ColorResources.WHITE,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Positioned(
                  top: 600.h,
                  left: 181.w,
                  child: ElevatedButton(
                      onPressed: () {
                        if (
                          controller.validateSubject(context)) {
                          controller.updateSubject(context);
                        }
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            ColorResources.YELLOW_BTN),
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        side: MaterialStateProperty.all<BorderSide>(
                          BorderSide(
                              color: ColorResources.BLUE_DEEP, width: 1.0.w),
                        ),
                        minimumSize:
                            MaterialStateProperty.all<Size>(Size(76.w, 40.h)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.21.r),
                          ),
                        ),
                      ),
                      child: Text('Save', style: TextStyle(fontSize: 10.sp)))),
              Positioned(
                  top: 600.h,
                  left: 271.w,
                  child: ElevatedButton(
                      onPressed: () {
                        controller.changeStateShowSubjectEditPage(false);
                        controller.resetSubjectForm();
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            ColorResources.BG_BLUE_COLOR),
                        foregroundColor: MaterialStateProperty.all<Color>(
                            ColorResources.WHITE),
                        side: MaterialStateProperty.all<BorderSide>(
                          BorderSide(color: ColorResources.WHITE, width: 1.0.w),
                        ),
                        minimumSize:
                            MaterialStateProperty.all<Size>(Size(76.w, 40.h)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.21.r),
                          ),
                        ),
                      ),
                      child:
                          Text('Cancel', style: TextStyle(fontSize: 10.sp)))),
              Positioned(
                top: 500.h,
                left: 23.w,
                child: SingleChildScrollView(
                  child: Container(
                    height: 100.h,
                    padding: EdgeInsets.only(top: 16.h),
                    child: controller.registeredCourses.isEmpty
                        ? Column(
                            children: [
                              Text(
                                  'This student doesn’t have any subject. Add one!!',
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      color: ColorResources.GREY)),
                              SizedBox(height: 8.h),
                              IconButton(
                                padding: const EdgeInsets.all(0),
                                icon: Icon(
                                  CupertinoIcons.add_circled_solid,
                                  color: ColorResources.WHITE,
                                  size: 28.dm,
                                ),
                                onPressed: openCourseSelectionDialog,
                              ),
                            ],
                          )
                        : SizedBox(
                            height: 80.h,
                            width: 313.w,
                            child: Wrap(
                                //alignment: WrapAlignment.start,
                                spacing: 14.0.h,
                                runSpacing: 8.0.w,
                                children: [
                                  ...controller.registeredCourses.map((course) {
                                    return Container(
                                      height: 30.h,
                                      width: 88.w,
                                      alignment: Alignment.center,
                                      //padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        color: ColorResources.GREY_MESSGES,
                                        borderRadius:
                                            BorderRadius.circular(30.r),
                                      ),
                                      child: Text(
                                        course,
                                        style: TextStyle(
                                            color: ColorResources.BLUE_BLACK,
                                            fontSize: 10.sp,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  IconButton(
                                    padding: const EdgeInsets.all(0),
                                    icon: Icon(
                                      CupertinoIcons.add_circled_solid,
                                      color: ColorResources.WHITE,
                                      size: 28.dm,
                                    ),
                                    onPressed: openCourseSelectionDialog,
                                  ),
                                ]),
                          ),
                  ),
                ),
              )
            ],
          ));
    });
  }
}
