import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:template/core/utils/color_resources.dart';
import 'package:flutter/cupertino.dart';
import 'package:template/data/model/subject/subject_model.dart';
import 'package:template/presentation/pages/home/home_controller.dart';
import 'package:template/presentation/pages/home/widgets/subject/subject_item/subject_item.dart';

class SubjectsTab extends GetView<HomeController> {
  const SubjectsTab({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 23.w, right: 23.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text('List subjects',
                        style: TextStyle(
                          fontSize: 20.sp,
                          fontWeight: FontWeight.bold,
                          color: ColorResources.BLACK,
                        )),
                    GestureDetector(
                      onTap: () {
                        controller.changeStateShowSubjectAddPage(true);
                      },
                      child: Icon(
                        CupertinoIcons.add_circled,
                        color: ColorResources.YELLOW_FOCUS,
                        size: 15.w,
                      ),
                    ),
                  ],
                ),
                Obx(() => 
                DropdownButton<String>(
                  value: controller.selectedItem.value,
                  items: const [
                    DropdownMenuItem<String>(
                      value: 'View all',
                      child: Text(
                        'View all',
                        style: TextStyle(color: ColorResources.YELLOW_FOCUS),
                      ),
                    ),
                    DropdownMenuItem(
                      value: 'Option 2',
                      child: Text(
                        'Option 2',
                        style: TextStyle(color: ColorResources.YELLOW_FOCUS),
                      ),
                    ),
                    DropdownMenuItem(
                      value: 'Option 3',
                      child: Text(
                        'Option 3',
                        style: TextStyle(color: ColorResources.YELLOW_FOCUS),
                      ),
                    )
                  ],
                  onChanged: controller.handleChangeDropDownButton,
                )
                )
              ],
            ),
          ),
          Obx(() => SizedBox(
            height: 485.h,
            child: 
            (controller.isLoadingSubject.value)
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : 
                ListView.separated(
                    padding: const EdgeInsets.all(0),
                    scrollDirection: Axis.vertical,
                    separatorBuilder: (_, __) => const SizedBox(
                      height: 17,
                    ),
                    itemCount: controller.subjectList.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      Subject subject = controller.subjectList[index];
                      return SubjectItem(subject: subject);
                    },
                  ),
          )),
        ],
      );
  }
}
