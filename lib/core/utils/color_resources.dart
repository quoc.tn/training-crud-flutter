// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

mixin ColorResources {
  //
  static const Color GREY_BG = Color(0xffF3F4F6);
  static const Color YELLOW_BTN = Color(0xffFFD566);
  static const Color YELLOW_TEXT = Color(0xffEDAC02);

  // Color alert.
  static const Color BG_BLUE_COLOR = Color(0xff0D205B);
  static const Color BG_COLOR = Color(0xff002184);
  static const Color BLUE_TITLE = Color(0xff96C0FF);
  static const Color ERROR_ALERT = Color(0xffDC4C64);
  static const Color SUCCESS_ALERT = Color(0xff14A44D);
  static const Color WARRING_ALERT = Color(0xffE4A11B);
  static const Color INFO_ALERT = Color(0xff54B4D3);
  static const Color CL_TEXT = Color(0xff535354);
  static const Color BG = Color(0xffF6F6F7);

  // Common.
  static const Color BACK_GROUND = Color(0xFF0A0A0A);
  static const Color BACK_GROUND_2 = Color(0xFFF6F6F7);
  static const Color BACK_GROUND_BOTTOM_CHAT = Color(0xFFF1F1F1);
  static const Color BACK_GROUND_INPUT_BOTTOM_CHAT = Color(0xFFF1F1F1);
  static const Color COLOR_DIALOG = Color(0XFF3B71CA);
  static const Color ORANGE = Color(0XFFFF813A);

  static const Color PRIMARY_1 = Color(0xff00498C);
  static const Color PRIMARY_2 = Color(0xff88E1E4);
  static const Color PRIMARY_3 = Color(0xffACFBFF);
  static const Color PRIMARY_4 = Color(0xffADA1F7);
  static const Color PRIMARY_5 = Color(0xff2EC57C);
  static const Color PRIMARY_6 = Color(0xffE5CF00);
  static const Color CHECK_SETUP = Color(0xffFF3877);

  static const Color BLUE_BLACK = Color(0xff000356);

  static const Color NEUTRALS_1 = Color(0xff212B38);
  static const Color NEUTRALS_2 = Color(0xff2C394A);
  static const Color NEUTRALS_3 = Color(0xff37465B);
  static const Color NEUTRALS_4 = Color(0xff727B86);
  static const Color NEUTRALS_5 = Color(0xffB9BDC3);
  static const Color NEUTRALS_6 = Color(0xffDCDEE1);
  static const Color NEUTRALS_7 = Color(0xffF3F5F9);
  static const Color NEUTRALS_8 = Color(0xff787878);
  static const Color NEUTRALS_9 = Color(0xff818181);
  static const Color NEUTRALS_10 = Color(0xffF9F9FA);
  static const Color COLOR_BUTTON = Color(0xffFF725E);
  static const Color DEEP_RED = Color(0xffF04C52);

  // ORIGIN COLOR
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color BLACK = Color(0xFF000000);
  static const Color BROW = Color(0xFF292929);
  static const Color LIGHT_BLACK = Color(0xFF191919);
  static const Color OUTER_SPACE = Color(0xFF464647); //
  static const Color BLACK_2 = Color(0xFF100737);
  static const Color BLACK_WEIGHT = Color(0xFF000000);
  static const Color LIGHT_GREY = Color(0xffBFBDBD);
  static const Color RED = Color(0xffF90B0B);
  static const Color GREY = Color(0xff8c8c8c);
  static const Color GREY_MESSGES = Color(0xffEDEDED);
  static const Color GREEN = Color(0xff23CB60);

  static const Color TEXT_PLACE_HOLDER = Color(0xFFB1B1B1);
  static const Color TEXT_BOLD = Color(0xFF181313);
  static const Color NICKEL = Color(0xFF677275);
  static const Color QUICK_SLIVER = Color(0xFFA4A2A2);
  static const Color QUICK_SLIVER_2 = Color(0xFFA4A4A2);

  static const Color YELLOW = Color(0xFFFCBF23);
  static const Color PINK = Color(0xFFD12C7C);
  static const Color SLIVER_CHALICE = Color(0xFFACACAC);
  static const Color PLATINUM = Color(0xFFE4E4E4);
  static const Color LIME_GREEN = Color(0xFF28B446);
  static const Color YELLOW_GREEN = Color(0xFF1ECF0F);
  static const Color BOSTON_RED = Color(0xFFCF0606);
  static const Color METALLIC_ORANGE = Color(0xFFDD710D);
  static const Color FULVOUS_ORANGE = Color(0xFFE9750B);
  static const Color BEER_ORANGE = Color(0xFFFC8B23);
  static const Color ELECTRIC_RED = Color(0xFFDE0000);
  static const Color DEEP_PINK = Color(0xFFEF2C2C);
  static const Color LIGHT_BLUE = Color(0xFF0075FF);
  static const Color YELLOW_FOCUS = Color(0xFFEDAC02);

  // Select language.
  static const Color LANGUAGE_UN_SELECT = Color(0xFF0D272D);
  static const Color ORANGE_DEEP = Color(0xFFFF4D00);
  static const Color BORDERBUTTON = Color(0xFFC8C8C8);
  static const Color BG_BUTTON = Color(0xFFFBFBFB);
  static const Color BORDER_BUTTON = Color(0xFFB1B1B1);
  static const Color ORANGE_LIGHT = Color(0xFFFFD8BC);
  static const Color GREEN_SELL = Color(0xFF0B8A4D);

  static const Color COLOR_E36D6D = Color(0xFFE36D6D);
  static const Color COLOR_E9330B = Color(0xFFE9330B);
  static const Color FIRE = Color(0xFFFF7C44);
  static const Color ORANGE_DEEP_2 = Color(0xFFFF7C44);
  static const Color YELLOW_DEEP = Color(0xFFECD400);
  static const Color colorBody = Color(0xff535354);

  static const Color TEXT_TITLE_COLOR = Color(0xFF464647);
  static const Color COLOR_FBD408 = Color(0xFFFBD408);
  static const Color YELLOW_LIGHT = Color(0xFFF4F27E);
  static const Color BLUE_DEEP = Color(0xFF00498C);
  static const Color PINK_LIGHT = Color(0xFFF0D6E4);
  static const Color GREEN_LIGHT = Color(0xFFDFFBBF);
  static const Color GREEN_DEEP = Color(0xFF259329);
  static const Color GREY_LIGHT = Color(0xFFF7F7F7);
  static const Color BLUE_LIGHT = Color(0xFFD4EBFF);
  static const Color BLUE_SUPERLIGHT = Color(0xFF22D7FF);
  static const Color GREEN2 = Color(0xFFCEFFE8);
  static const Color BLUE2 = Color(0xFF3889D3);
}
