

import 'package:get_it/get_it.dart';
import 'package:template/data/repositories/student_repository.dart';
import 'package:template/data/repositories/subject_respository.dart';
final sl = GetIt.instance;
Future<void> init() async {
  sl.registerLazySingleton<StudentRespository>(()=>
  StudentRespository());
  sl.registerLazySingleton<SubjectRespository>(()=>
  SubjectRespository());
  }
