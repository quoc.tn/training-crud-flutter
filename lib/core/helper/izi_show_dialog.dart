import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../utils/color_resources.dart';

mixin IziShowDialog {
  static void showSnackBar(BuildContext context, String title, Color color) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: color,
        content: Text(title,
            style: TextStyle(color: ColorResources.WHITE, fontSize: 16.sp)),
        action: SnackBarAction(
          label: 'Close',
          onPressed: scaffold.hideCurrentSnackBar,
          textColor: ColorResources.WHITE,
        ),
      ),
    );
  }
}