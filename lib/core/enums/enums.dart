// ignore_for_file: camel_case_types


enum TypeOfDevice {
  bigDevice,
  smallDevice,
  normalDevice,
  mediumSmallDevice,
}

enum OptionalMessageSelect {
  deleteMessage,
  editMessage,
}

enum StatusTypingMessage {
  typing,
  renderReply,
  none,
}

enum IZIButtonType {
  DEFAULT,
  OUTLINE,
}

enum IZIImageType {
  SVG,
  IMAGE,
  NOT_IMAGE,
}

enum IZIImageUrlType {
  NETWORK,
  ASSET,
  FILE,
  ICON,
  IMAGE_CIRCLE,
}

enum IZIInputType {
  TEXT,
  PASSWORD,
  NUMBER,
  DOUBLE,
  PRICE,
  EMAIL,
  PHONE,
  INCREMENT,
  MULTILINE,
  DATE,
}

enum IZIPickerDate {
  MATERIAL,
  CUPERTINO,
}

enum CONNECTION_STATUS {
  CONNECTED,
  DISCONNECTED,
}

enum TypeOfAlert {
  ERROR,
  SUCCESS,
  INFO,
  WARRING,
}

enum AppTypeEnum {
  A3_AI_CHAT,
  A1_VPS,
  A2_ASK_AI,
}

enum FIleTypeEnum {
  AUDIO,
  VIDEO,
}

enum OptionAudio {
  VOLUME,
  PITCH,
  SPEED,
}

enum TypeOfEffect {
  NORMAL,
  PREMIUM,
}

enum KeyTempFileType {
  DELETE_TEMP_FILE,
}

enum TypeGetOTP {
  SIGN_UP,
  FORGOT_PASSWORD,
  LOGIN,
  SOCIAL,
}

enum SocialType {
  NONE,
  GOOGLE,
  FACEBOOK,
  APPLE,
}

enum LoginTypeEnum { STORE, CUSTOMER }

enum VehicleType {
  TRUCK,
  CAR,
  MOTORBIKE,
}

extension MediaTypeExtension on VehicleType {
  String get stringValue {
    switch (this) {
      case VehicleType.CAR:
        return 'CAR';
      case VehicleType.MOTORBIKE:
        return 'MOTORBIKE';
      case VehicleType.TRUCK:
        return 'TRUCK';
      default:
        return '';
    }
  }
}

enum SelectedTabEnum {
  RESCUE,
  ALL,
  PRODUCT,
  CAR_SERVICE,
  INSURANCE,
}

enum TimeType {
  MONTH,
  DAYS,
  YEAR,
}

enum ProductTypeEnum {
  PRODUCT,
  INSURANCE,
  CAR_SERVICE,
  SUPPLIER,
}

enum DeliveryMethodsEnum {
  HOME_DELIVERY,
  SUPPLIER_PICKUP,
}

enum TargetTypeEnum {
  PERSONAL,
}

enum NotificationTypeEnum {
  DISCOUNT,
  USER_RESCUE,
  SERVICE,
  ORDER,
  CANCEL_ORDER,
  SYSTEM
}

enum EntityTypeEnum {
  DISCOUNT,
  USER_RESCUE,
  SERVICE,
  ORDER,
  SYSTEM,
  CANCEL_ORDER,
}

enum ReviewTypeEnum {
  CAR_SERVICE,
  PRODUCT,
  INSURANCE,
}

enum CartTab {
  ORDER,
  HISTORY,
}

enum OrderTypeEnum {
  PRODUCT,
  INSURANCE,
  CAR_SERVICE,
}

enum PaymentMethodEnum {
  COD,
  VNPAY,
  MOMO,
}

enum CancelledByEnum {
  CUSTOMER,
  SUPPLIER,
}

enum PaymentStatusEnum {
  PENDING,
  PAID,
  FAILED,
  REFUNDED,
}

enum OrderStatusEnum {
  PENDING,
  PROCESSING,
  DELIVERY,
  DELIVERED,
  CANCELLED,
  REFUNDED,
  RETURNED,
  FAILED,
  COMPLETED,
  RATE,
}

enum ApplyToEnum {
  ALL,
  SPECIFIC,
  BANK,
  FIRST_ORDER,
}

enum DurationInsuranceEnum {
  ONE_YEAR,
  TWO_YEAR,
}

enum LocationTypeEnum {
  HERE,
  FACTORY,
}

enum AppointmentAddress { ON_SITE, AT_SHOP }

enum ResonEnum {
  TheCarCannotStart,
  AccessibleWheels,
  BeFlooded,
  Accident,
  Other
}

enum RescuesEnum {
  FAILED,
  PENDING,
  CONFIRMED,
  COMPLETED,
  FIXING,
}

enum TypeFilePost {
  IMAGE,
  VIDEO,
}

enum TypeFileLocal {
  NONE,
  FILE,
}

enum AnimatedSwitchType {
  SCALE,
  FADE,
}

enum PreviewImage {
  PREVIEW,
  VIEW,
}

enum FileTypeEnum {
  IMAGE,
  VIDEO,
  UNKNOWN,
}


enum CustomerType{
  CUSTOMER,
  SUPPLIER
}