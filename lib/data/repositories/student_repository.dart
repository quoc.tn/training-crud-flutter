
import 'dart:convert';

import 'package:template/core/helper/izi_validate.dart';
import 'package:template/domain/end_points/end_points.dart';
import 'package:dio/dio.dart';
import 'package:template/data/export/data_export.dart';

class StudentRespository  {
  final dio = Dio();
  void get({
    String? filter,
    required Function(List<Student> list) onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      response = await dio.get( EndPoints.student);
    } catch (e) {
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) && response.statusCode! >= 200 && response.statusCode! <= 300) {
      final returnData = response.data as List<dynamic>;
      List<Student> students = [];
        for (var data in returnData) {
          students.add(Student.fromJson(data));
        }
      onSuccess(students);
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  void delete({
    required String id,
    required Function() onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      response = await dio.delete( '${EndPoints.student}/$id');
    } catch (e) {
      //print('Error delete data: $e');
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) && response.statusCode! >= 200 && response.statusCode! <= 300) {
      onSuccess();
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  void add({
    required Map<String, dynamic> data,
    required Function() onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      // print(data);
      response = await dio.post( EndPoints.student, data: jsonEncode(data));
      // print(response);
    } catch (e) {
      //print('Error delete data: $e');
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) && response.statusCode! >= 200 && response.statusCode! <= 300) {
      onSuccess();
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  void update({
    required String id,
    required Map<String, dynamic> data,
    required Function() onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      // print(data);
      response = await dio.put( '${EndPoints.student}/$id', data: jsonEncode(data));
      // print(response);
    } catch (e) {
      //print('Error delete data: $e');
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) && response.statusCode! >= 200 && response.statusCode! <= 300) {
      onSuccess();
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  
}
