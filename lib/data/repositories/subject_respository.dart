import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:template/core/helper/izi_validate.dart';
import 'package:template/data/data_source/exception/api_error_handler.dart';
import 'package:template/data/model/base/api_response.dart';
import 'package:template/data/model/subject/subject_model.dart';
import 'package:template/domain/end_points/end_points.dart';

class SubjectRespository {
  final dio = Dio();

  void get({
    String? filter,
    required Function(List<Subject> list) onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      response = await dio.get(EndPoints.subject);
      print(response);
    } catch (e) {
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) &&
        response.statusCode! >= 200 &&
        response.statusCode! <= 300) {
      final returnData = response.data as List<dynamic>;
      List<Subject> subjects = [];
        for (var data in returnData) {
          subjects.add(Subject.fromJson(data));
        }
      onSuccess(subjects);
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  void delete({
    required String id,
    required Function() onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      response = await dio.delete('${EndPoints.subject}/$id');
    } catch (e) {
      //print('Error delete data: $e');
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) &&
        response.statusCode! >= 200 &&
        response.statusCode! <= 300) {
      onSuccess();
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  void add({
    required Map<String, dynamic> data,
    required Function() onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
       print(jsonEncode(data));
      response = await dio.post( EndPoints.subject, data: jsonEncode(data));
       //print(response);
    } catch (e) {
      //print('Error delete data: $e');
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) && response.statusCode! >= 200 && response.statusCode! <= 300) {
      onSuccess();
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }

  
  void update({
    required String id,
    required Map<String, dynamic> data,
    required Function() onSuccess,
    required Function(dynamic error) onError,
  }) async {
    late Response response;
    try {
      // print(data);
      response = await dio.put( '${EndPoints.subject}/$id', data: jsonEncode(data));
      // print(response);
    } catch (e) {
      //print('Error delete data: $e');
      onError(ApiResponse.withError(ApiErrorHandler.getMessage(e)).error);
      return;
    }
    if (!IZIValidate.nullOrEmpty(response.statusCode) && response.statusCode! >= 200 && response.statusCode! <= 300) {
      onSuccess();
    } else {
      onError(ApiErrorHandler.getMessage(response.data));
    }
  }
}
