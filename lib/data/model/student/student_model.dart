List<Student> studentsFromJson(List<dynamic> jsonList) {
  List<Student> students = [];
  for (var json in jsonList) {
    students.add(Student.fromJson(json));
  }
  return students;
}

class Student {
  final String id;
  final String fullName;
  final String className;
  final num dateOfBirth;
  final num averageScore;
  final List<String> registeredCourses;
  final ContactInfo contactInfo;  

  Student(
     this.id, 
     this.fullName, 
     this.className, 
     this.dateOfBirth,
     this.averageScore, 
     this.registeredCourses, 
     this.contactInfo);

  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
      json['_id'],
      json['fullName'] ?? '',
      json['class'] ?? '',
      json['dateOfBirth'] ?? 0,
      json['averageScore'] ?? '',
      List<String>.from(json['registeredCourses']),
      ContactInfo.fromJson(json['contactInfo']),
    );
  }
}

class ContactInfo {
  final String id;
  final String email;
  final String phoneNumber;
  final String address;

  ContactInfo(
    this.id,
    this.email,
    this.phoneNumber,
    this.address
  );

  factory ContactInfo.fromJson(Map<String, dynamic> json) {
    return ContactInfo(
      json['_id'],
      json['email'],
      json['phoneNumber'],
      json['address'],
    );
  }
}
