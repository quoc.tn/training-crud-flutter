class SubjectList {
  final List<Subject> subjects;
  SubjectList(this.subjects);
  factory SubjectList.fromJson(List<dynamic> json) {
    List<Subject> subjects = json.map((studentJson) {
      return Subject.fromJson(studentJson);
    }).toList();
    return SubjectList(subjects);
  }
}

class Subject {

 final String id;
 final String registrationStatus;
 final num creditHours;
 final String semester;
 final List<String> registeredCourses;
 final String className;
 final String fullName;
 final String studentId;

  Subject(
    this.id,
    this.registrationStatus,
    this.creditHours,
    this.semester,
    this.registeredCourses,
    this.className,
    this.fullName,
    this.studentId,
  );

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'registrationStatusm': registrationStatus,
      'creditHours': creditHours,
      'semester': semester,
      'registeredCourses': registeredCourses,
      'className': className,
      'fullName': fullName,
      'StudentId': studentId,
    };
  }

  factory Subject.fromJson(Map<String, dynamic> json) {
    return Subject(
      json['_id'],
      json['registrationStatus'] ?? '',
      json['creditHours'] ?? 0,
      json['semester']??'',
      List<String>.from(json['registeredCourses'] ?? []),
      json['class'] ?? '',
      json['fullName'] ?? '',
      json['studentId'] ?? '',
    );
  }
}
