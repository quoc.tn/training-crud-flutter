class EndPoints {
    static const String BASE_URL = 'https://training.izisoft.io/';
    static const String student = 'https://training.izisoft.io/v1/students';
    static const String subject = 'https://training.izisoft.io/v1/course-registrations';

}