import 'route_path/main_routers.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

mixin AppPages {
  static List<GetPage> list = [
    ...MainRouters.listPage,
  ];
}