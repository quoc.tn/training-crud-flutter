import 'package:get/get.dart';
import 'package:template/presentation/pages/main_layout/main_layout_page.dart';
import 'package:template/presentation/pages/main_layout/main_layout_binding.dart';

mixin MainRouters {
  // ignore: constant_identifier_names
  static const String MAIN_LAY_OUT = '/main';
  static List<GetPage> listPage = [
    // GetPage(name: DETAIL_ACCOUNT, page: () => const DetailHomePage()),
    GetPage(
      name: MAIN_LAY_OUT,
      page: () => const MainLayout(),
      binding: MainLayoutBinding(),
      transition: Transition.rightToLeft,
    )
  ];
}