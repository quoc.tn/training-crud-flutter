
import 'package:flutter/material.dart';
import 'core/di_container.dart' as di;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'core/export/core_export.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:template/config/routes/app_pages.dart';
import 'config/export/config_export.dart';
import 'app_binding.dart';
Future<void> main() async {

  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
        return ScreenUtilInit(
      ensureScreenSize: true,
      minTextAdapt: true,
      splitScreenMode: true,
      designSize: const Size(375, 812),
      // designSize: context.isTablet
      //     ? Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height)
      //     : const Size(360, 800),
      useInheritedMediaQuery: true,
      builder: (context, index) {
        return GestureDetector(
          // onTap: () {
          //   FocusManager.instance.primaryFocus?.unfocus();
          // },
          child: GetMaterialApp(
            initialRoute: MainRouters.MAIN_LAY_OUT,
            initialBinding: AppBinding(),
            defaultTransition: Transition.leftToRight,
            transitionDuration: const Duration(),
            getPages: AppPages.list,
            debugShowCheckedModeBanner: false,
            theme: AppTheme.light,
            localizationsDelegates: localizationsDelegates,
            // supportedLocales: LocalizationService.locales,
            // fallbackLocale: LocalizationService.fallbackLocale,
            // translations: LocalizationService(),
            // locale: LocalizationService.locale,
            builder: EasyLoading.init(
              builder: (context, widget) {
                return Theme(
                  data: lightTheme,
                  child: MediaQuery(
                    data: MediaQuery.of(context).copyWith(
                      textScaleFactor: 1.0,
                      boldText: false,
                    ),
                    child: widget!,
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}
